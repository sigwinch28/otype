-module(otype_log).
-behaviour(gen_server).

-export([start_link/1]).
-export([init/1,handle_call/3,handle_cast/2]).
-export([set_file/1,log/1,log/2,log/3]).

-define(SERVER, otype_log).

start_link(Debug) ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, Debug, []).

init(Debug) ->
    State = #{debug => Debug,
	      filename => ""},
    {ok, State}.


set_file(Filename) ->
    gen_server:call(?SERVER, {file, Filename}).

log(Fmt) ->
    gen_server:call(?SERVER, {log, Fmt, none, []}).


log(Fmt, Args) when is_list(Args) ->
    gen_server:call(?SERVER, {log, Fmt, none, Args});
log(Fmt, Line) ->
    gen_server:call(?SERVER, {log, Fmt, Line, []}).


log(Fmt, Line, Args) ->
    gen_server:call(?SERVER, {log, Fmt, Line, Args}).


handle_call({file, Filename}, _From, State) ->
    {reply, ok, State#{ filename := Filename}};
handle_call({log, Fmt, none, Args}, _From, #{filename:=Filename, debug:=true} = State) ->
    io:format("(otype) ~s: " ++ Fmt, [Filename|Args]),
    {reply, ok, State};
handle_call({log, Fmt, Line, Args}, _From, #{filename:=Filename, debug:=true} = State) ->
    io:format("(otype) ~s:~p: " ++ Fmt, [Filename, Line] ++ Args),
    {reply, ok, State};
handle_call(_, _From, State) ->
    {reply, ok, State}.


handle_cast(_, State) ->
    {noreply, State}.
