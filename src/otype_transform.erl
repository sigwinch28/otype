
-module(otype_transform).

-export([parse_transform/2,parse_transform/3]).

-import(otype_abstract, [get_attrs/1,get_module_attr/1,get_behaviour_attrs/1, get_functions/1,
			find_function/2,merge_forms/2]).
-import(otype_log, [log/1,log/2,log/3]).


parse_transform(Forms, ErlOptions, Debug) ->
    parse_transform(Forms, [{otype_debug,Debug}|ErlOptions]).


parse_transform(Forms, ErlOptions) ->
    Attrs = get_attrs(Forms),
    Module = get_module_attr(Attrs),
    Debug = proplists:get_bool(otype_debug, ErlOptions),
    otype_log:start_link(Debug),
    otype_log:set_file(atom_to_list(Module) ++ ".erl"),
    Behaviours = get_behaviour_attrs(Attrs),
    KnownBehaviours = restrict_to_known_behaviours(Behaviours),
    case KnownBehaviours of
	[] ->
	    log("Not analysing module ~0p as it does not contain any typed behaviours~n", [Module]),
	    Forms;
	_ ->
	    foo,
	    otype_log:start_link(Debug),

	    log("Analysing module ~0p as it contains typed behaviours~n", [Module]),
	    {ok, Module, Core, _Warnings} = ast_to_core(Forms),
	    log("Typed behaviours: ~0p~n", [KnownBehaviours]),
	    Callbacks = all_typed_callbacks(KnownBehaviours),
	    log("All callbacks for typed behaviours: ~0p~n", [Callbacks]),
	    Types = all_function_types(Core, Callbacks),
	    [ log("Function ~0p/~0p has type ~0p~n", [Name,Arity,Type]) || {{Name,Arity}, Type} <- Types],
	    NewForms = predefined_functions(Types),
	    log("otype_info/1 injected and exported in module ~0p~n", [Module]),
	    merge_forms(NewForms, Forms)
    end.


%%
%% Compilation
%%

%%
%% @doc Add the compilation options to compile (and return) core erlang to an
%% existing list of compiler options.
%%
-spec add_core_compile_options([term()]) -> [term()].
add_core_compile_options(Options) ->
    [to_core,verbose,return|Options].

%%
%% @doc Normalise the return value of the `erlang:compile' functions so that
%% empty lists of errors and warnings are returned when the result of compilation
%% is just the bare atom `error'.
%%
%% @see erlang:compile/2
-spec normalise_compile_ret({ok, atom(), term(), term()} | error | {error, term(), term()}) ->
				   {ok, atom(), term(), term()} | {error, term(), term()}.
normalise_compile_ret({ok, Mod, Res, Warnings}) -> {ok, Mod,  Res, Warnings};
normalise_compile_ret(error) -> {error, [], []};
normalise_compile_ret({error, Errors, Warnings}) -> {error, Errors, Warnings}.

ast_to_core(AST) ->
    ast_to_core(AST, []).

ast_to_core(AST, Options) ->
    CoreOptions = add_core_compile_options(Options),
    normalise_compile_ret(compile:forms(AST, CoreOptions)).

%%
%% Behaviour information
%%

all_known_behaviours() ->
    [otype_gen_server].

is_known_behaviour(Behaviour) ->
    lists:member(Behaviour, all_known_behaviours()).

restrict_to_known_behaviours(Behaviours) ->
    [Behaviour || Behaviour <- Behaviours, is_known_behaviour(Behaviour)].

%%
%% Behaviour callback information
%%

behaviour_typed_callbacks(otype_gen_server) ->
    [{handle_call,3},{handle_cast,2},{handle_info,2}].

all_typed_callbacks(Behaviours) ->
    Sets = [sets:from_list(behaviour_typed_callbacks(Behaviour)) || Behaviour <- Behaviours],
    sets:to_list(sets:union(Sets)).

%%
%% Message argument positions
%%

%message_args_for_behaviour_callbacks(otype_gen_server) ->
%    [{handle_call,1}, {handle_cast,1}, {handle_info,1}].

%%
%% Function types
%%

all_function_types(_Module, []) -> [];
all_function_types(Module, CallbackNames) ->
    Defs = cerl:module_defs(Module),
    lists:filtermap(fun({VarName,Fun}) ->
		      Name = cerl:var_name(VarName),
		      case lists:member(Name, CallbackNames) of
			  true ->
			      Body = cerl:fun_body(Fun),
			      case cerl:type(Body) of
				  'case' ->
				      Types = cerl_case_types(Body, 1),
				      {true, {Name, Types}};
				  _ ->
				      %% in this case we assume no pattern matching
				      {true, {Name, [term]}}
			      end;
			  false ->
			      false
		      end
		    end, Defs).


cerl_case_types(Case, Arg) ->
    Clauses = cerl:case_clauses(Case),
    Clauses1 = lists:filter(fun(C) -> not cerl_is_compiler_generated(C) end, Clauses),
    lists:map(fun(Clause) ->
		      Patterns = cerl:clause_pats(Clause),
		      Pattern = lists:nth(Arg, Patterns),
		      pattern_types(Pattern)
	      end, Clauses1).

pattern_types(Tree) ->
    case cerl:type(Tree) of
	literal ->
	    Value = cerl:concrete(Tree),
	    otype_type:type_of(Value);
	tuple ->
	    Es = cerl:tuple_es(Tree),
	    Types = lists:map(fun pattern_types/1, Es),
	    {tuple, Types};
	var ->
	    term
    end.

cerl_is_compiler_generated(Tree) ->
    lists:member(compiler_generated, cerl:get_ann(Tree)).

%% get_otype_options(Options) ->
%%     case lists:keyfind(otype, 1, Options) of
%% 	{_, Opts} ->
%% 	    Opts;
%% 	false ->
%% 	    []
%%     end.

%% get_otype_option(Name, Options) ->
%%     {_, Value}  = lists:keyfind(Name, 1, Options),
%%     Value.

%% get_otype_option_default(Name, Options, Default) ->
%%     case lists:keyfind(Name, 1, Options) of
%% 	{_, Value} ->
%% 	    Value;
%% 	false ->
%% 	    Default
%%     end.


term_to_abstract(Term) ->
    Options = [{encoding, none}],
    erl_parse:abstract(Term, Options).


predefined_functions([]) -> [];
predefined_functions(Types) ->
    Forms = predefined_type_function(Types),
    Annotated = [erl_parse:new_anno(Form) || Form <- Forms],
    Export = [{Name,Arity} || {function,_,Name,Arity,_} <- Annotated],
    [{attribute,0,export,Export}|Annotated].


predefined_type_function(Types) ->
    [{function,0,otype_info,1,
      lists:map(fun predefined_type_function_clause/1, Types)}].

predefined_type_function_clause({Name,Types}) ->
    {clause,0,[term_to_abstract(Name)],[],[term_to_abstract(Types)]}.
