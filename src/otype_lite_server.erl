-module(otype_lite_server).

-callback init(Args :: term()) -> {ok, State :: term()}.

-callback handle_call(Request :: term(), From :: {pid(), term()}, State :: term()) ->
    {ok, Reply :: term(), NewState :: term()}.

-callback handle_case(Request :: term(), State :: term()) ->
    {noreply, NewState :: term()}.
