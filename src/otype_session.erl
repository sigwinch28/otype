-module(otype_session).

-type direction() :: 'in' | 'out'.
-type transition() :: {direction(), otype_type:type(), atom()}.
-type session() :: #{atom() => [transition()]}.

-type option(A) :: {'some', A} | 'none'.

-export_type([direction/0, transition/0, session/0, option/1]).

-export([next_state_name/4]).

%% @doc If an option type is a `some', the provided function F will be applied
%% to the `some's value, and wrapped up in an option type. Returns 'none'
%% otherwise.
-spec map_option(fun((A) -> B), option(A)) -> option(B).
map_option(_F, none) -> none;
map_option(F, {some, X}) -> {some, F(X)}.

%% @doc Returns the first element of a list matching a predicate function as
%% an option type. Returns `{some, A}' for the first element of the list for
%% which F(A) returns true. Returns `none' otherwise.
-spec first(fun((A) -> boolean()), [A]) -> option(A).
first(_F, []) ->
    none;
first(F, [X|Xs]) ->
    case F(X) of
	true -> {some, X};
	false -> first(F, Xs)
    end.

%% @doc Gets the list of transitions in a session for the given state name.
-spec state_transitions(atom(), session()) -> [transition()].
state_transitions(CurrentState, Session) ->
    maps:get(CurrentState, Session).

%% @doc Returns the next state name of the provided transition.
-spec transition_state_name(transition()) -> atom().
transition_state_name({_Direction,_Type,Name}) ->
    Name.

%% @doc Returns true if the provided transition is "compatible" with the
%% provided direction and type. A transition is considered compatible iff the
%% direction is the same, and when
-spec is_compatible_transition(direction(), otype_type:type(), transition()) -> boolean().
is_compatible_transition(Direction, Type, {Direction, TransType, _Name}) ->
    otype:is_subtype(Type, TransType);
is_compatible_transition(_Direction, _Type, {_DifferentDirection, _TransTy, _Name}) ->
    false.

-spec first_compatible_transition(direction(), otype_type:type(), [transition()]) -> option(transition()).
first_compatible_transition(Direction, Type, Transitions) ->
    first(fun(Transition) -> is_compatible_transition(Direction, Type, Transition) end, Transitions).

-spec next_state_name(atom(), direction(), otype_type:type(), session()) -> option(atom()).
next_state_name(CurrentStateName, MsgDirection, MsgType, Session) ->
    Transitions = state_transitions(CurrentStateName, Session),
    MaybeTransition = first_compatible_transition(MsgDirection, MsgType, Transitions),
    map_option(fun transition_state_name/1, MaybeTransition).
