-module(otype).

%% API exports

-export([options_from_gen_options/1, types/1, compile_types_enabled/1,
	 client_errors_enabled/1,callback_types/3,module_compile_types/3,
	 all_types/4,term_to_type/1,is_subtype/2,is_subtype_any/2,type_of/1]).

%%====================================================================
%% API functions
%%====================================================================

options_from_gen_options(GenServerOptions) ->
    case lists:keyfind(otype, 1, GenServerOptions) of
	{_,OtypeOptions} -> OtypeOptions;
	false -> []
    end.

types(Options) ->
    maps:get(types, Options, []).

compile_types_enabled(Options) ->
    maps:get(compile_types, Options, true).

client_errors_enabled(Options) ->
    maps:get(client_errors, Options, false).

%session(Options) ->
%    maps:get(session, Options, none).

%has_session(Options) ->
%    session(Options) =/= none.

%state(Options) ->
%    maps:get(state, Options, '$undefined').

%update_state(State, Options) ->
%    Options#{state => State}.

callback_types(Name, Arity, Options) ->
    case types(Options) of
	[] -> [];
	Callbacks ->
	    maps:get({Name, Arity}, Callbacks, [])
    end.

module_compile_types(Mod, Name, Arity) ->
    case erlang:function_exported(Mod, otype_info, 1) of
	true ->
	    try Mod:otype_info({Name,Arity}) of
		Type -> [Type]
	    catch
		error:function_clause -> []
	    end;
	false ->
	    []
    end.

all_types(Mod, Name, Arity, Options) ->
    CompileTypes = case compile_types_enabled(Options) of
		       true -> module_compile_types(Mod, Name, Arity);
		       false -> []
		   end,
    CallbackTypes = callback_types(Name, Arity, Options),
    CompileTypes ++ CallbackTypes.

term_to_type(Term) ->
    otype_type:type_of(Term).

type_of(Term) ->
    otype_type:type_of(Term).

is_subtype(T1, T2) ->
    otype_type:is_subtype(T1, T2).

is_subtype_any(_, []) ->
    true;
is_subtype_any(Type, Types) ->
    lists:any(fun(Ty) ->
		      otype_type:is_subtype(Type, Ty)
	      end, Types).

%%====================================================================
%% Internal functions
%%====================================================================
