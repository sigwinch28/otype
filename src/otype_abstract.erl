%%
%% @doc A library to deal with specific portions of the Erlang Abstract Format,
%% for extracting attributes from a list of forms, finding function definitions,
%% and merging new forms into a list of existing forms which will pass the
%% linter in future compiler passes.
%%
-module(otype_abstract).

-export_types([form/0, function_name/0]).

-export([get_attrs/1, find_attr/2, get_module_attr/1, get_behaviour_attrs/1,
	 get_functions/1, find_function/2, merge_forms/2]).

-type form() :: erl_parse:abstract_form() | erl_parse:form_info().
-type attribute() :: {atom(), term()}.

%%
%% @doc Retrieves the attributes defined in a list of Erlang Abstract Forms
%% (for example, from a module), and returns them as a list of key-value pairs.
%%
-spec get_attrs([form()]) -> [attribute()].
get_attrs(Forms) ->
    [{Name, Val} || {attribute, _Location, Name, Val} <- Forms].

%%
%% @doc Find an attribute by name in a list of key-value pairs, where the key
%% is the name of the attribute. Returns a list of all values, in the same order
%% as the input list.
-spec find_attr(atom(), [attribute()]) -> [term()].
find_attr(Name, Attrs) ->
    [Value || {Name1, Value} <- Attrs, Name =:= Name1].

%%
%% @doc Find the first attribute by name in a list of key-value pairs, where the
%% key is the name of the attribute. Returns the first pair, or false.
%%
-spec find_attr1(atom(), [attribute()]) -> term() | false.
find_attr1(Name, Attrs) ->
    lists:keyfind(Name, 1, Attrs).

%%
%% @doc Determine the name of the module defined by the list of attributes. This
%% searches for the module attribute within the list.
%%
-spec get_module_attr([attribute()]) -> atom() | false.
get_module_attr(Attrs) ->
    {module, Module} = find_attr1(module, Attrs),
    Module.

%%
%% @doc Gets a list of behaviour names from a list of attributes.
%%
-spec get_behaviour_attrs([attribute()]) -> [atom()].
get_behaviour_attrs(Attrs) ->
    find_attr(behaviour, Attrs).

%%
%% @doc Retrieve a list of functions defined in a list of forms.
%% This filters each `function' abstract form, and converts it into a triple of
%% function name, arity, and the original form representing the function.
%%
-spec get_functions([form()]) -> [{atom(), pos_integer(), form()}].
get_functions(Forms) ->
    [{Name, Arity, Form} || {function, _Location, Name, Arity, _Body} = Form <- Forms].

%%
%% @doc Find a function with the given name and arity within a list of functions
%% returned by `get_functions/1'. Returns false if no function is found.
%%
%% @see get_functions/1
-spec find_function({atom(), pos_integer()}, [{atom(), pos_integer(), [form()]}]) -> {atom(), pos_integer(), [form()]} | false.
find_function({_Name, _Arity}, []) ->
    false;
find_function({Name, Arity}, [{Name, Arity, _Form} = Function|_Functions]) ->
    Function;
find_function({Name, Arity}, [_|Functions]) ->
    find_function({Name, Arity}, Functions).

%%
%% @doc Compares two erlang abstract forms, and makes attributes leq functions,
%% and functions leq anything else.
%%
form_leq({attribute,_,_,_}, {attribute,_,_,_}) -> false;
form_leq({attribute,_,_,_}, _) -> true;
form_leq({function,_,_,_,_}, {function,_,_,_,_}) -> false;
form_leq({function,_,_,_,_}, {attribute,_,_,_}) -> false;
form_leq({function,_,_,_,_}, _) -> true.

%%
%% @doc Merge two lists of forms.
%% This exists because we generate new forms before we reach the linter, which
%% does not allow the definition of attributes after a function.
%%
merge_forms(Forms1, Forms2) ->
    lists:merge(fun form_leq/2, Forms1, Forms2).
