-module(otype_transform_debug).

-export([parse_transform/2]).


parse_transform(Forms, ErlOptions) ->
    otype_transform:parse_transform(Forms, ErlOptions, true).
