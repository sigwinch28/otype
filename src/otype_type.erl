-module(otype_type).

%% API exports
-export([is_subtype/2,type_of/1]).

-type type() ::
  'term' | 'none'
 | 'atom' | 'boolean'
 | 'number' | 'float' | 'integer'
 | 'tuple'
 | {'tuple', [type()]}
 | 'list'
 | 'nil' | {'cons', type(), type()}
 | {'union', type(), type()}
 | {'literal',term(),type()}.

%%====================================================================
%% API functions
%%====================================================================

%% @doc Determines the type of an Erlang term.
-spec type_of(term()) -> type().
type_of(X) when is_boolean(X) -> {literal, X, boolean};
type_of(X) when is_atom(X) -> {literal, X, atom};
type_of(X) when is_float(X) -> {literal, X, float};
type_of(X) when is_integer(X) -> {literal, X, integer};
type_of(X) when is_number(X) -> {literal, X, number};
type_of(X) when is_tuple(X) -> {tuple,
 [type_of(E) || E <- tuple_to_list(X)]};
type_of([]) -> nil;
type_of([X|Xs]) -> {cons, type_of(X), type_of(Xs)}.



%% @doc Returns true if the first type is a subtype of the second type. Returns
%% false otherwise. This relation is symmetric.
-spec is_subtype(type(), type()) -> boolean.

is_subtype({literal,V1,T1}, {literal,V2,T2}) ->
    is_subtype(T1, T2) andalso V1 == V2;
is_subtype({literal,_V,T1}, T2) ->
    is_subtype(T1, T2);
is_subtype(_, {literal, _V, _T}) ->
    false;

is_subtype({union, T1, T2}, T3) ->
    is_subtype(T1, T3) andalso is_subtype(T2, T3);
is_subtype(T1, {union, T2, T3}) ->
    is_subtype(T1, T2) orelse is_subtype(T1, T3);

is_subtype(none, none) -> true;
is_subtype(_, none) -> false;

is_subtype(_, term) -> true;

is_subtype(boolean, boolean) -> true;
is_subtype(_, boolean) -> false;

is_subtype(atom, atom) -> true;
is_subtype(X, atom) -> is_subtype(X, boolean);

is_subtype(integer, integer) -> true;
is_subtype(_, integer) -> false;

is_subtype(float, float) -> true;
is_subtype(_, float) -> false;

is_subtype(number, number) -> true;
is_subtype(X, number) -> is_subtype(X, integer) or is_subtype(X, float);

is_subtype(tuple, tuple) -> true;
is_subtype({tuple, _Xs}, tuple) -> true;
is_subtype(_, tuple) -> false;

is_subtype({tuple, Xs}, {tuple, Ys}) ->
    length(Xs) == length(Ys) andalso
	lists:all(fun id/1, lists:zipwith(fun is_subtype/2, Xs, Ys));
is_subtype(_, {tuple, _}) -> false;

is_subtype(list, list) -> true;
is_subtype(nil, list) -> true;
is_subtype({cons, _X, _Xs}, list) -> true;

is_subtype(nil, nil) -> true;
is_subtype(_, nil) -> false;

is_subtype({cons, X, Xs}, {cons, Y, Ys}) ->
    is_subtype(X, Xs) andalso is_subtype(Y, Ys);
is_subtype(_, {cons, _, _}) -> false.



%%====================================================================
%% Internal functions
%%====================================================================
id(X) -> X.
