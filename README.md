# OTyPe

Type-safe OTP behaviours with optional error propagation to callers.

# Currently Supported Behaviours

There is preliminary support for the following OTP behaviours

## `gen_server` (as `otype_gen_server`)

To use this behaviour, add the following to your module, similar to if you were
writing a `gen_server`:

```erlang
-behaviour(otype_gen_server).
```

Furthermore, where you would use the `gen_server` module (for example,
`gen_server:cast`), use the same function from `otype_gen_server`:

```erlang
alloc(Server, Ch) ->
	otype_gen_server:call(Server, {alloc, Ch}).
```

# Adding types to behaviours

By default, the behaviours in this package behave identically to those from the
OTP library. If you want to perform runtime type checking on incoming messages,
you must choose one of the following options:

* **Compile-time type inference**: types will be inferred automatically at
  compile time.
* **TODO: Use type specifications**
* **Manual type specification at spawn**: types are explicitly provided when
  starting the behaviour.

## Compile-time type inference

To automatically infer the types of messages accepted by the behaviour
callbacks in a module, specify the `otype_transform` parse transform in your
module:

```erlang
-compile([{parse_transform, otype_transform}]).
```

or somewhere else, for example in your `rebar.config`:

```erlang
{erl_opts, [{parse_transform, otype_transform}]}.
```

If your module implements a behaviour which is known to OTyPe (i.e. when you
specify an `otype_*` behaviour), it will be analysed during compilation. Each
callback function will have its arguments analysed: the argument which
corresponds to the message received will have its type inferred.

These inferred types will be made available at runtime via the automatic
injection of the `otype_info/1` function. This function returns the inferred
type of a callback's message argument, e.g.
`my_server:otype_info({handle_call,3})`.

### Disabling use of compile-time type inference

If a module has been compiled with the parse transform, the statically inferred
types will automatically be used at runtime. To disable this functionality, pass
`no_compile_types` in the `otype` options when starting the behaviour:


```erlang
otype_gen_server:start_link(?MODULE, [], [{otype, [no_compile_types]}]).
```

## TODO: Use type specifications

Idea: use user-provided type specifications to cache static type information.
Perhaps gradualizer could be used for this.

## Manual type specification at spawn

A list of types may be specified manually on start via the `otype` option.
For example, to specify the type of `handle_call/3` function as `atom` for
a `gen_server`:

```erlang
Types = {{handle_call, 3}, [atom]},
Opts = [{otype, [{types, Types}]}],
otype_gen_server:start_link(?MODULE, [], Opts).
```

# Error reports

If types have been provided for a callback, all messages received in the
behaviour will have their types checked _before_ being passed to their
corresponding callback function. If the message type _is not_ a subtype of the
callback types, then the message will be discarded and an error report will
be generated. For example:

```
1> {ok, Pid} = otype_gen_server:start_link(test_server_one, [], []),
{ok,<0.121.0>}
2> otype_gen_server:call(Pid, {bad_tuple}).
noreply
** Message of wrong type for test_server_one:handle_call/3 received
** Message with wrong type: {bad_tuple,2}
** Message type: {tuple,[{literal,atom,bad_tuple},{literal,integer,2}]}
3>
```

The server will continue to process messages after discarding the message with
the incompatible type.

If you wish, you may force error onto the client via the `client_errors` option:

```
1> {ok, Pid} = otype_gen_server:start_link(test_server_one, [], [{otype, [client_errors]}]),
{ok,<0.121.0>}
2> otype_gen_server:call(Pid, {bad_tuple}).
** Message of wrong type for test_server_one:handle_call/3 received
** Message with wrong type: {bad_tuple,2}
** Message type: {tuple,[{literal,atom,bad_tuple},{literal,integer,2}]}

** exception exit: {{wrong_message_type,{tuple,[{literal,atom,bad_tuple},
                                                {literal,integer,2}]}},
                    {otype_gen_server,call,[<0.121.0>,{bad_tuple,2}]}}
     in function  otype_gen_server:call/2 (otype_gen_server.erl, line 215)
3>
```

This does not work with with calls to `cast`, as the sender of the message is
not known at the time of processing, and the sender may have already continued
execution.
