-module(otype_gen_server_SUITE).
-compile([export_all,nowarn_export_all]).

-include_lib("stdlib/include/assert.hrl").
-define(SERVER, test_server_one).


all() ->
    [
     test_handle_call_good_alloc,
     test_handle_call_good_alloc_types,
     test_handle_call_good_alloc_session,
     test_handle_call_bad_alloc,
     test_handle_call_bad_alloc_types,
     test_handle_call_bad_alloc_session,
     test_handle_cast_good_free,
     test_handle_cast_good_free_types,
     test_handle_cast_good_free_session,
     test_handle_cast_bad_free,
     test_handle_cast_bad_free_types,
     test_handle_cast_bad_free_session
    ].

init_per_suite(Config) ->
    {module, ?SERVER} = code:load_file(?SERVER),
    Config.

end_per_suite(_Config) ->
    ok.


%% Helpers =====================================================================
no_compile_types() ->
    without_compile_types(#{}).

session() ->
    Session = #{session =>
		    #{'s0' => [{'in', {tuple, [{literal,alloc,atom},term]}, 's1'},
			       {'in', {tuple, [{literal,free,atom},term]}, 's0'}],
		      's1' => [{'out', {literal,in_use,atom}, 's0'},
			       {'out', {literal,ok,atom}, 's0'}]
		     }
	       },
    without_compile_types(Session#{state => 's0'}).

next_state_name(Msg, Dir, Opts) ->
    Session = otype:session(Opts),
    CurState = otype:state(Opts),
    Type = otype:term_to_type(Msg),
    {some, Name} = otype_session:next_state_name(CurState, Dir, Type, Session),
    Name.

without_compile_types(Options) ->
    Options#{compile_types => false}.

type_opts(Callback) ->
    Types = case Callback of
		{handle_call,3} ->  [{tuple, [{literal,alloc,atom},term]}];
		{handle_cast, 2} -> [{tuple, [{literal,free,atom},term]}]
	    end,
    Options = #{types => #{Callback => Types}},
    without_compile_types(Options).



try_handle_call(Message) ->
    Opts = no_compile_types(),
    try_handle_call(Message, Opts).

try_handle_call(Message, Opts) ->
    otype_gen_server:try_handle_call(?SERVER, Message, undefined, [], Opts).

try_handle_cast(Message) ->
    Opts = no_compile_types(),
    try_handle_cast(Message, Opts).

try_handle_cast(Message, Opts) ->
    otype_gen_server:try_dispatch({'$gen_cast', Message}, ?SERVER, [], Opts).

%% Test Cases ==================================================================

%% @doc A well-typed message is handled as expected when no types are provided
test_handle_call_good_alloc(_) ->
    Msg = {alloc, 2},
    Opts = no_compile_types(),
    {ok, _, Opts} = try_handle_call(Msg, Opts).

%% @doc A well-typed message is handled as expected when types are provided
test_handle_call_good_alloc_types(_) ->
    Msg = {alloc, 2},
    Opts = type_opts({handle_call,3}),
    {ok, _, _} = try_handle_call(Msg, Opts).

%% @doc A well-typed message is handled as expected when a session is provided,
%% and the resulting state in the server is the same as from the session library
test_handle_call_good_alloc_session(_) ->
    Msg = {alloc, 2},
    Opts = session(),
    Next = next_state_name(Msg, 'in', Opts),
    {ok, _, NewOpts} = try_handle_call(Msg, Opts),
    ?assertEqual(Next, otype:state(NewOpts)).

%% @doc A badly-typed message causes an 'EXIT' in the callback, and this is
%% returned when no types are provided.
test_handle_call_bad_alloc(_) ->
    Msg = {bad_alloc, 2},
    Opts = no_compile_types(),
    {'EXIT', error, function_clause, _} = try_handle_call(Msg, Opts).

%% @doc A badly-typed message causes a type error to be returned, and therefore
%% the original callback is never called.
test_handle_call_bad_alloc_types(_) ->
    Msg = {bad_alloc, 2},
    Type = otype:term_to_type(Msg),
    Opts = type_opts({handle_call,3}),
    {type_error, {handle_call, 3}, Msg, Type} = try_handle_call(Msg, Opts).

%% @doc A badly-typed message causes a session error to be returned, and
%% therefore the original callback is never called, and the session state does
%% not change
test_handle_call_bad_alloc_session(_) ->
    Msg = {bad_alloc, 2},
    Type = otype:term_to_type(Msg),
    Opts = session(),
    State = otype:state(Opts),
    {no_next_state,State,in,Msg,Type} = try_handle_call(Msg, Opts).

%% @doc A well-typed message is handled as expected when no types are provided
test_handle_cast_good_free(_) ->
    Msg = {free, 2},
    {ok, _, _} = try_handle_cast(Msg).

%% @doc A well-typed message is handled as expected when types are provided
test_handle_cast_good_free_types(_) ->
    Msg = {free, 2},
    Opts = type_opts({handle_cast,2}),
    {ok, _, _} = try_handle_cast(Msg, Opts).

%% @doc A well-typed message is handled as expected when a session is provided,
%% and the new state matches the output of the session library.
test_handle_cast_good_free_session(_) ->
    Msg = {free, 2},
    Opts = session(),
    Next = next_state_name(Msg, 'in', Opts),
    {ok, _, NewOpts} = try_handle_cast(Msg, Opts),
    ?assertEqual(Next, otype:state(NewOpts)).


test_handle_cast_bad_free(_) ->
    Msg = {bad_free, 2},
    Opts = no_compile_types(),
    {'EXIT', error, function_clause, _} = try_handle_cast(Msg, Opts).

test_handle_cast_bad_free_types(_) ->
    Msg = {bad_free, 2},
    Opts = type_opts({handle_cast,2}),
    Type = otype:term_to_type(Msg),
    {type_error, {handle_cast, 2}, Msg, Type} = try_handle_cast(Msg, Opts).

test_handle_cast_bad_free_session(_) ->
    Msg = {bad_free, 2},
    Type = otype:term_to_type(Msg),
    Opts = session(),
    State = otype:state(Opts),
    {no_next_state,State,in,Msg,Type} = try_handle_cast(Msg, Opts).
