-module(otype_transform_SUITE).
-compile([export_all,nowarn_export_all]).

-define(SERVER, test_server_one).

all() ->
    [test_transform_server,
     test_otype_info_server].


compile() ->
    Opts = [{parse_transform,otype_transform},return],
    {ok, Dir} = file:get_cwd(),
    Filename = filename:join(Dir, atom_to_list(?SERVER) ++ ".erl"),
    {ok,?SERVER,_,[]} = compile:file(Filename, Opts).


test_transform_server(_Config) ->
    ok.


test_otype_info_server(_Config) ->
    erlang:function_exported(?SERVER, otype_info, 1),

    erlang:function_exported(?SERVER, handle_call, 3) andalso
	?SERVER:otype_info({handle_call,3}),

    erlang:function_exported(?SERVER, handle_cast, 2) andalso
	?SERVER:otype_info({handle_cast,2}),

    ok.
