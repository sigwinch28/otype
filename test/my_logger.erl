-module(my_logger).
-compile([{parse_transform,otype_transform_debug}]).
-behaviour(otype_gen_server).
-vsn("1").

-export([start_link/0]).
-export([push/0,pop/0,set_depth/1,log/1]).
-export([init/1, handle_call/3, handle_cast/2]).
%-export([otype_info/1]).

-define(NAME, my_logger).

start_link() ->
    otype_gen_server:start_link({local, ?NAME}, ?MODULE, [], []).

push() ->
    otype_gen_server:call(?NAME, push).

pop() ->
    otype_gen_server:call(?NAME, pop).

set_depth(Depth) ->
    otype_gen_server:call(?NAME, {depth, Depth}).

log(Str) ->
    otype_gen_server:call(?NAME, {log, Str}).

init(_Args) ->
    {ok, 0}.

handle_call(push, _From, Depth) ->
    {reply, ok, Depth+1};
handle_call(pop, _From, Depth) ->
    {reply, ok, Depth-1};
handle_call({depth, Depth}, _From, _Depth) ->
    {reply, ok, Depth};
handle_call({log, Str}, _From, Depth) ->
    Indent = lists:duplicate(Depth, $\ ),
    io:format("~s~n", [[Indent, Str]]),
    {reply, ok, Depth}.

handle_cast(_, State) ->
    {noreply, State}.

%otype_info({handle_call,3}) ->
%    [{literal,push,atom},
%     {literal,pop,atom},
%     {tuple, [{literal,depth,atom},term]},
%     {tuple, [{literal,log,atom},list]}];
%otype_info({handle_cast,2}) ->
%    [none].
