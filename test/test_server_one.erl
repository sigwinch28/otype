-module(test_server_one).
-compile([{parse_transform,otype_transform_debug}]).

-behaviour(otype_gen_server).

-export([start_link/0,alloc/2,free/2]).
-export([init/1,handle_call/3,handle_cast/2]).

start_link() ->
    otype_gen_server:start_link(?MODULE, [], []).

alloc(Pid, Ch) ->
    otype_gen_server:call(Pid, {alloc, Ch}).

free(Pid, Ch) ->
    otype_gen_server:cast(Pid, {free, Ch}).


init(_) ->
    {ok, []}.

handle_call({alloc, Ch}, _From, Chs) ->
    case lists:member(Ch, Chs) of
	true -> {reply, in_use, Chs};
	false -> {reply, ok, [Ch|Chs]}
    end.

handle_cast({free, Ch}, Chs) ->
    {noreply, lists:delete(Ch, Chs)}.
